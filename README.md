## Website
The project is available to view at: [A synaesthetic visualiser
for classic barbershop tags](https://vinhphamvan.pythonanywhere.com/)

![Front Page](front_page.png)

# Description
This repository accompanies the code at: [Synaesthetic VIsualiser for Barbershop Tags](https://gitlab.com/vinhphamvan/synaesthetic-visualiser-for-barbershop-tags)

Here you can find the code used to do the pre-processing of the input data.

The project uses Flask to build a colourful graphical visualisation of the 125 classic barbershop tags found at https://www.barbershoptags.com/Classic-Tags.

The CREPE pitch tracker was used to detect the frequency of the 4 parts that make up the harmony, and then was visualised by mapping these frequencies to a 2d plane with each frequency representing a height and a color based on the chromesthesia of Alexander Scriabin.

![Visualiser](visualiser.png)