###############################################
# Imports
###############################################
import json
import math

###############################################
# Get general tag data
###############################################
with open('json/tag_data/tag_data.json') as tag_data_file:
    tag_data = json.load(tag_data_file)

    for tag in tag_data:

        print('Processing tag: ' + str(tag['Title']) + " - " + str(tag['id']))

        # Read corresponding pitch data
        with open('json/tag_data/pitch_data/' + str(tag['id']) + '.json') as pitch_data_file:
            pitch_data = json.load(pitch_data_file)
            for part in pitch_data:
                time = part['time']
                bass_pitches = part['bass_pitches']
                bari_pitches = part['bari_pitches']
                lead_pitches = part['lead_pitches']
                tenor_pitches = part['tenor_pitches']

        # Read corresponding color data
        with open('json/tag_data/color_data/' + str(tag['id']) + '.json') as color_data_file:
            color_data = json.load(color_data_file)

            for part in color_data:
                bass_colors = part['bass_colors']
                bari_colors = part['bari_colors']
                lead_colors = part['lead_colors']
                tenor_colors = part['tenor_colors']

        # Generate collective data
        chart_data = {}

        ###############################################
        # Threshold analysed pitch at normal human range
        ###############################################
        bass_data = []
        for i in range(len(time)):
            bass_data.append({'value': (0.0 if bass_pitches[i] == 0.0 else math.log2(bass_pitches[i])) if (
                    bass_pitches[i] < 1046.50) else math.log2(1046.50), 'color': bass_colors[i] if (
                    bass_pitches[i] < 1046.50) else "#000000"})

        bari_data = []
        for i in range(len(time)):
            bari_data.append({'value': (0.0 if bari_pitches[i] == 0.0 else math.log2(bari_pitches[i])) if (
                    bari_pitches[i] < 1046.50) else math.log2(1046.50), 'color': bari_colors[i] if (
                    bari_pitches[i] < 1046.50) else "#000000"})

        lead_data = []
        for i in range(len(time)):
            lead_data.append({'value': (0.0 if lead_pitches[i] == 0.0 else math.log2(lead_pitches[i])) if (
                    lead_pitches[i] < 1046.50) else math.log2(1046.50), 'color': lead_colors[i] if (
                    lead_pitches[i] < 1046.50) else "#000000"})

        tenor_data = []
        for i in range(len(time)):
            tenor_data.append({'value': (0.0 if tenor_pitches[i] == 0.0 else math.log2(tenor_pitches[i])) if (
                    tenor_pitches[i] < 1046.50) else math.log2(1046.50), 'color': tenor_colors[i] if (
                    tenor_pitches[i] < 1046.50) else "#000000"})

        ###############################################
        # Save collective data in json files
        ###############################################
        chart_data['id'] = tag['id']
        chart_data['Title'] = tag['Title']
        chart_data['AltTitle'] = tag['AltTitle']
        chart_data['WritKey'] = tag['WritKey']
        chart_data['Rating'] = tag['Rating']
        chart_data['SheetMusic'] = tag['SheetMusic']
        chart_data['video_count'] = tag['videos']
        chart_data['videos_list'] = tag['videos_list']
        chart_data['duration'] = time[-1]
        chart_data['bass_data'] = bass_data
        chart_data['bari_data'] = bari_data
        chart_data['lead_data'] = lead_data
        chart_data['tenor_data'] = tenor_data

        with open('json/tag_data/collective_data/' + str(tag['id']) + '.json', 'w') as f:
            json.dump(chart_data, f, indent=4)
