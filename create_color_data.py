###############################################
# Imports
###############################################
import json
from pathlib import Path

###############################################
# File Locations
###############################################
tag_data_src_file = 'json/tag_data/tag_data.json'

###############################################
# Get color mappings
###############################################
with open('json/mappings/extended_mappings.json') as mappings_file:
    mappings_data = json.load(mappings_file)


###############################################
# Get color of a given pitch
###############################################
def get_color(pitch):

    for pitch_index in range(len(mappings_data)):
        if pitch < mappings_data[0]['threshold']:
            return '#000000'
        elif pitch > mappings_data[pitch_index]['threshold']:
            continue
        else:
            return mappings_data[pitch_index - 1]['color_code']


###############################################
# Generate color data for all tags
###############################################
with open('json/tag_data/tag_data.json') as tag_data_file:
    tag_data = json.load(tag_data_file)
    for element in tag_data:

        if not Path('json/tag_data/color_data/' + str(element['id']) + '.json').is_file():

            print('Generating color_data for tag: ' + element['Title'] + ' (' + str(element['id']) + ')')

            with open('json/tag_data/pitch_data/' + str(element['id']) + '.json') as pitch_data_file:
                pitch_data = json.load(pitch_data_file)

                bass_colors = []
                bari_colors = []
                lead_colors = []
                tenor_colors = []

                for data_arrays in pitch_data:

                    for i in range(len(data_arrays['time'])):
                        bass_colors.append(get_color(data_arrays['bass_pitches'][i]))
                        bari_colors.append(get_color(data_arrays['bari_pitches'][i]))
                        lead_colors.append(get_color(data_arrays['lead_pitches'][i]))
                        tenor_colors.append(get_color(data_arrays['tenor_pitches'][i]))

            color_data = [{'bass_colors': bass_colors
                           , 'bari_colors': bari_colors
                           , 'lead_colors': lead_colors
                           , 'tenor_colors': tenor_colors}]

            with open('json/tag_data/color_data/' + str(element['id']) + '.json', 'w') as f:
                json.dump(color_data, f, indent=4)
