###############################################
# Imports
###############################################
from pydub import AudioSegment
import json
import math
import crepe
from scipy.io import wavfile

###############################################
# File locations
###############################################
bass_mp3_dst = "audio/demo/Bass.mp3"
bari_mp3_dst = "audio/demo/Bari.mp3"
lead_mp3_dst = "audio/demo/Lead.mp3"
tenor_mp3_dst = "audio/demo/Tenor.mp3"

bass_wav_dst = "audio/demo/Bass.wav"
bari_wav_dst = "audio/demo/Bari.wav"
lead_wav_dst = "audio/demo/Lead.wav"
tenor_wav_dst = "audio/demo/Tenor.wav"


###############################################
# Get color mappings
###############################################
with open('json/mappings/extended_mappings.json') as mappings_file:
    mappings_data = json.load(mappings_file)


###############################################
# Get color of a given pitch
###############################################
def get_color(pitch):

    for pitch_index in range(len(mappings_data)):
        if pitch < mappings_data[0]['threshold']:
            return '#000000'
        elif pitch > mappings_data[pitch_index]['threshold']:
            continue
        else:
            return mappings_data[pitch_index - 1]['color_code']


###############################################
# Analyse demo files
###############################################
print("Converting to wav...")
AudioSegment.from_file(bass_mp3_dst).export(bass_wav_dst, format="wav")
AudioSegment.from_file(bari_mp3_dst).export(bari_wav_dst, format="wav")
AudioSegment.from_file(lead_mp3_dst).export(lead_wav_dst, format="wav")
AudioSegment.from_file(tenor_mp3_dst).export(tenor_wav_dst, format="wav")


###############################################
# Create pitch data
###############################################
print("Analysing pitch...")
sr, audio = wavfile.read(bass_wav_dst)
time, bass_pitches = crepe.predict(audio, sr, viterbi=True)[:2]
sr, audio = wavfile.read(bari_wav_dst)
bari_pitches = crepe.predict(audio, sr, viterbi=True)[1]
sr, audio = wavfile.read(lead_wav_dst)
lead_pitches = crepe.predict(audio, sr, viterbi=True)[1]
sr, audio = wavfile.read(tenor_wav_dst)
tenor_pitches = crepe.predict(audio, sr, viterbi=True)[1]


###############################################
# Create color data
###############################################
print('Creating color_data...')

bass_colors = []
bari_colors = []
lead_colors = []
tenor_colors = []

for i in range(len(time)):
    bass_colors.append(get_color(bass_pitches[i]))
    bari_colors.append(get_color(bari_pitches[i]))
    lead_colors.append(get_color(lead_pitches[i]))
    tenor_colors.append(get_color(tenor_pitches[i]))


###############################################
# Generate collective data
###############################################
collective_data = {}

###############################################
# Threshold analysed pitch at normal human range
###############################################
print('Thresholding...')

bass_data = []
for i in range(len(time)):
    bass_data.append({'value': (0.0 if bass_pitches[i] == 0.0 else math.log2(bass_pitches[i])) if (
            bass_pitches[i] < 1046.50) else math.log2(1046.50), 'color': bass_colors[i] if (
            bass_pitches[i] < 1046.50) else "#000000"})

bari_data = []
for i in range(len(time)):
    bari_data.append({'value': (0.0 if bari_pitches[i] == 0.0 else math.log2(bari_pitches[i])) if (
            bari_pitches[i] < 1046.50) else math.log2(1046.50), 'color': bari_colors[i] if (
            bari_pitches[i] < 1046.50) else "#000000"})

lead_data = []
for i in range(len(time)):
    lead_data.append({'value': (0.0 if lead_pitches[i] == 0.0 else math.log2(lead_pitches[i])) if (
            lead_pitches[i] < 1046.50) else math.log2(1046.50), 'color': lead_colors[i] if (
            lead_pitches[i] < 1046.50) else "#000000"})

tenor_data = []
for i in range(len(time)):
    tenor_data.append({'value': (0.0 if tenor_pitches[i] == 0.0 else math.log2(tenor_pitches[i])) if (
            tenor_pitches[i] < 1046.50) else math.log2(1046.50), 'color': tenor_colors[i] if (
            tenor_pitches[i] < 1046.50) else "#000000"})


print('Creating collective data file...')
collective_data['id'] = 'demo'
collective_data['Title'] = 'Demo'
collective_data['duration'] = time[-1]
collective_data['bass_data'] = bass_data
collective_data['bari_data'] = bari_data
collective_data['lead_data'] = lead_data
collective_data['tenor_data'] = tenor_data

with open('json/tag_data/collective_data/demo.json', 'w') as f:
    json.dump(collective_data, f, indent=4)
