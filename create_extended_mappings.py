###############################################
# Imports
###############################################
import json
from scipy.ndimage import convolve1d

###############################################
# File locations
###############################################
mappings_src_file = 'json/mappings/mappings.json'
extended_mappings_src_file = 'json/mappings/extended_mappings.json'

###############################################
# Extend mappings to 4 octaves
###############################################
pitches = []
color_codes = []

pitches.append(61.74)

with open(mappings_src_file) as json_file:
    data = json.load(json_file)
    for i in range(0, 4):
        for p in data:
            pitches.append(p['pitch'] * pow(2, i))
            color_codes.append(p['color_code'])

pitches.append(1046.50)
color_codes.append('#ffffff')

###############################################
# Create thresholds from mappings
###############################################
average_kernel = [0.5, 0.5]
thresholds = convolve1d(pitches, average_kernel)[:-1].tolist()

###############################################
# Save extended mapping data
###############################################
extended_mappings = []

for i in range(len(thresholds)):
    extended_mappings.append({'threshold': thresholds[i], 'color_code': color_codes[i]})

with open(extended_mappings_src_file, 'w') as f:
    json.dump(extended_mappings, f, indent=4)
