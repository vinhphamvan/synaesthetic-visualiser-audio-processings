###############################################
# Imports
###############################################
import json
import urllib.request
from pydub import AudioSegment
import crepe
from scipy.io import wavfile
from pathlib import Path

###############################################
# File locations
###############################################
tag_data_src_file = 'json/tag_data/tag_data.json'

bass_mp3_dst = "audio/mp3/Bass.mp3"
bari_mp3_dst = "audio/mp3/Bari.mp3"
lead_mp3_dst = "audio/mp3/Lead.mp3"
tenor_mp3_dst = "audio/mp3/Tenor.mp3"

bass_wav_dst = "audio/wav/Bass.wav"
bari_wav_dst = "audio/wav/Bari.wav"
lead_wav_dst = "audio/wav/Lead.wav"
tenor_wav_dst = "audio/wav/Tenor.wav"


###############################################
# Create pitch data for each tag
###############################################
with open(tag_data_src_file) as json_file:
    data = json.load(json_file)
    for element in data:

        if not Path('json/tag_data/pitch_data/' + str(element['id']) + '.json').is_file():

            # Request mp3 file
            print("Retrieving mp3 files for tag: " + element['Title'] + "(" + str(element['id']) + ")...")
            urllib.request.urlretrieve(element['Bass'], bass_mp3_dst)
            urllib.request.urlretrieve(element['Bari'], bari_mp3_dst)
            urllib.request.urlretrieve(element['Lead'], lead_mp3_dst)
            urllib.request.urlretrieve(element['Tenor'], tenor_mp3_dst)

            # Extract right side of dominant tracks and convert to wav
            print("Converting to wav...")
            AudioSegment.from_file(bass_mp3_dst).split_to_mono()[1].export(bass_wav_dst, format="wav")
            AudioSegment.from_file(bari_mp3_dst).split_to_mono()[1].export(bari_wav_dst, format="wav")
            AudioSegment.from_file(lead_mp3_dst).split_to_mono()[1].export(lead_wav_dst, format="wav")
            AudioSegment.from_file(tenor_mp3_dst).split_to_mono()[1].export(tenor_wav_dst, format="wav")

            # Perform the pitch analysis using CREPE
            print("Analysing pitch...")
            sr, audio = wavfile.read(bass_wav_dst)
            time, bass_frequency = crepe.predict(audio, sr, viterbi=True)[:2]
            sr, audio = wavfile.read(bari_wav_dst)
            bari_frequency = crepe.predict(audio, sr, viterbi=True)[1]
            sr, audio = wavfile.read(lead_wav_dst)
            lead_frequency = crepe.predict(audio, sr, viterbi=True)[1]
            sr, audio = wavfile.read(tenor_wav_dst)
            tenor_frequency = crepe.predict(audio, sr, viterbi=True)[1]

            # Save timing and pitch data in json files
            pitch_data = [{'time': time.tolist()
                           , 'bass_pitches': bass_frequency.tolist()
                           , 'bari_pitches': bari_frequency.tolist()
                           , 'lead_pitches': lead_frequency.tolist()
                           , 'tenor_pitches': tenor_frequency.tolist()}]

            with open('json/tag_data/pitch_data/' + str(element['id']) + '.json', 'w') as f:
                json.dump(pitch_data, f, indent=4)
