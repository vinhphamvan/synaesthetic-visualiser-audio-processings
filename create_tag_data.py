###############################################
# Imports
###############################################
import requests
import xml.etree.ElementTree as ElementTree
import json

###############################################
# File locations
###############################################
tag_data_src_file = 'json/tag_data/tag_data.json'


###############################################
# Get a given video's details
###############################################
def get_video_details(video_list):

    video_details = []

    for video in video_list:

        video_detail = {}

        for properties in video:
            if properties.tag == 'Code':
                video_detail['Code'] = properties.text
            elif properties.tag == 'SungBy':
                video_detail['SungBy'] = properties.text
            elif properties.tag == 'Posted':
                video_detail['Posted'] = properties.text

        video_details.append(video_detail)

    return video_details


###############################################
# Get the properties of a given tag
###############################################
def get_properties(input):
    
    tag_properties = []

    for properties in input:

        if properties.tag == 'id':
            tag_properties.append(int(properties.text))

        elif properties.tag == 'WritKey':

            key = ''
            if properties.text.split(':')[0] == 'Major':
                key = properties.text.split(':')[1]
            if properties.text.split(':')[0] == 'Minor':
                key = properties.text.split(':')[1] + " min"
            tag_properties.append(key)

        elif properties.tag == 'Rating':
            tag_properties.append(round(float(properties.text), 2))

        elif properties.tag == 'SheetMusicAlt':
            continue

        elif properties.tag == 'videos':
            tag_properties.append(properties.attrib['available'])
            tag_properties.append(get_video_details(properties))

        else:
            tag_properties.append(properties.text)

    return tag_properties


###############################################
# Perform a request through barbershoptags API
# for all 125 Classic Tags
###############################################
search_url = "http://www.barbershoptags.com/api.php"
search_url += "?Collection=classic&n=125"
search_url += "&fldlist=id,Title,AltTitle,WritKey,Rating,SheetMusic,Bass,Bari,Lead,Tenor,AllParts,videos"

response = requests.get(search_url)
root = ElementTree.fromstring(response.content)


###############################################
# Save data in json file
###############################################
tag_data = []
for tag in root:
    tag_properties = get_properties(tag)
    dictionary = {'id': tag_properties[0]
                  , 'Title': tag_properties[1]
                  , 'AltTitle': tag_properties[2]
                  , 'WritKey': tag_properties[3]
                  , 'Rating': tag_properties[4]
                  , 'SheetMusic': tag_properties[5]
                  , 'Bass': tag_properties[6]
                  , 'Bari': tag_properties[7]
                  , 'Lead': tag_properties[8]
                  , 'Tenor': tag_properties[9]
                  , 'AllParts': tag_properties[10]
                  , 'videos': tag_properties[11]
                  , 'videos_list': tag_properties[12]}

    tag_data.append(dictionary)

with open(tag_data_src_file, 'w') as f:
    json.dump(tag_data, f, indent=4)

