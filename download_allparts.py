###############################################
# Imports
###############################################
import json
import urllib.request
from pathlib import Path

###############################################
# File locations
###############################################
tag_data_src_file = 'json/tag_data/tag_data.json'

###############################################
# Download full mixes for all 125 tags
###############################################
with open(tag_data_src_file) as json_file:
    data = json.load(json_file)
    for element in data:

        if not Path('audio/allparts/' + str(element['id']) + ".mp3").is_file():
            print("Retrieving mp3 files for tag: " + element['Title'] + "(" + str(element['id']) + ")...")

            urllib.request.urlretrieve(element['AllParts'], 'audio/allparts/' + str(element['id']) + ".mp3")
