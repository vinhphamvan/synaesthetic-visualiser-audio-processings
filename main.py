###############################################
# Imports
###############################################
import json
import numpy as np
import matplotlib.pyplot as plt

###############################################
# Output pitch and color data using matplotlib
###############################################
fig, ax = plt.subplots(figsize=(45, 15))

with open('json/tag_data/pitch_data/31.json') as pitches_file:
    pitch_data = json.load(pitches_file)
    for element in pitch_data:
        time = element['time']
        bass_pitches = element['bass_pitches']
        bari_pitches = element['bari_pitches']
        lead_pitches = element['lead_pitches']
        tenor_pitches = element['tenor_pitches']

with open('json/tag_data/color_data/31.json') as colors_file:
    color_data = json.load(colors_file)
    for element in color_data:
        bass_colors = np.array([int(x[1:], 16) for x in element['bass_colors']])
        bari_colors = np.array([int(x[1:], 16) for x in element['bari_colors']])
        lead_colors = np.array([int(x[1:], 16) for x in element['lead_colors']])
        tenor_colors = np.array([int(x[1:], 16) for x in element['tenor_colors']])

###############################################
# Use ax.scatter to represent both pitch and color
###############################################
ax.scatter(time, bass_pitches, c=bass_colors, s=100)
ax.scatter(time, bari_pitches, c=bari_colors, s=100)
ax.scatter(time, lead_pitches, c=lead_colors, s=100)
ax.scatter(time, tenor_pitches, c=tenor_colors, s=100)
ax.set_xlabel("Time (seconds)")
ax.set_ylabel("Frequency")
ax.grid(True)
ax.set_title("Pitches")

plt.show()
